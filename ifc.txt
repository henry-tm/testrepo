eth0      Link encap:Ethernet  HWaddr 08:00:27:12:77:c7  
          inet addr:192.168.1.78  Bcast:192.168.1.255  Mask:255.255.255.0
          inet6 addr: fe80::a00:27ff:fe12:77c7/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:51137 errors:0 dropped:0 overruns:0 frame:0
          TX packets:9593 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000 
          RX bytes:6406191 (6.1 MiB)  TX bytes:1224953 (1.1 MiB)

lo        Link encap:Local Loopback  
          inet addr:127.0.0.1  Mask:255.0.0.0
          inet6 addr: ::1/128 Scope:Host
          UP LOOPBACK RUNNING  MTU:65536  Metric:1
          RX packets:0 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0 
          RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)

             total       used       free     shared    buffers     cached
Mem:          494M       343M       150M       4,7M        27M       245M
-/+ buffers/cache:        70M       423M
Swap:         1,5G         0B       1,5G
